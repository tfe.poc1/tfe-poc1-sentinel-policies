## POC Brief
https://drive.google.com/file/d/1h-cHxfIxyQGpfMDbNul3pKFw6l50ScRg/view?usp=sharing

## Here is the draft architecture diagram
https://drive.google.com/file/d/1DWMwLFvCYXFCe_lYV6D09NtrSuONc-0A/view?usp=sharing

## Sentinel Policies requirements:
https://docs.google.com/spreadsheets/d/1I_LxttjcP5qnZXa2NTbggq0eZ1wy4wnSa9zBtCy1qlM/edit?usp=sharing

## Instructions for Gitlab

### SSH to Gitlab commands

### Generate an SSH key pair to prevent the CLI asking for a password at every commit 
## Use this reference guide here for more details https://docs.gitlab.com/ee/user/ssh.html
ssh-keygen -t ed25519 -C "<comment>"

1. Press Enter. Output similar to the following is displayed:Generating public/private ed25519 key pair.
2. Enter file in which to save the key (/home/user/.ssh/id_ed25519):
3. Accept the suggested filename and directory, unless you are generating a deploy key or want to save in a specific directory where you store other keys.You can also 
dedicate the SSH key pair to a specific host.
4. Specify a passphrase:Enter passphrase (empty for no passphrase):
5. Enter same passphrase again:
Add an SSH key to your GitLab account
tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy

1. Replace id_ed25519.pub with your filename. For example, use id_rsa.pub for RSA.
2. Sign in to GitLab.
3. On the top bar, in the top right corner, select your avatar.
4. Select Preferences.
5. On the left sidebar, select SSH Keys.
6. In the Key box, paste the contents of your public key. If you manually copied the key, make sure you copy the entire key, which starts with ssh-rsa, ssh-dss, 
ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519, sk-ecdsa-sha2-nistp256@openssh.com, or sk-ssh-ed25519@openssh.com, and may end with a comment.
7. In the Title box, type a description, like Work Laptop or Home Workstation.
8. Optional. Update Expiration date to modify the default expiration date. In:
    * GitLab 13.12 and earlier, the expiration date is informational only. It doesn’t prevent you from using the key. Administrators can view expiration dates and use them 
for guidance when deleting keys.
    * GitLab checks all SSH keys at 02:00 AM UTC every day. It emails an expiration notice for all SSH keys that expire on the current date. (Introduced in GitLab 13.11.)
    * GitLab checks all SSH keys at 01:00 AM UTC every day. It emails an expiration notice for all SSH keys that are scheduled to expire seven days from now. (Introduced in 
GitLab 13.11.)
9. Select Add key.

Verify that you can connect
ssh -T git@gitlab.example.com


## Gitlab Useful Command line instructions on your local machine
You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "Your Name"

git config --global user.email "Your Email address"

### Create a new repository
git clone git@gitlab.com:tfe.poc1/tfe-poc1-sentinel-policies.git

cd tfe-poc1-sentinel-policies

git switch -c main

touch README.md

git add README.md

git commit -m "add README"

git push -u origin main

## Push an existing folder
cd existing_folder

git init --initial-branch=main

git remote add origin git@gitlab.com:tfe.poc1/tfe-poc1-sentinel-policies.git

git add .

git commit -m "Initial commit"

git push -u origin main

### Push an existing Git repository
cd existing_repo

git remote rename origin old-origin

git remote add origin git@gitlab.com:tfe.poc1/tfe-poc1-sentinel-policies.git

git push -u origin --all

git push -u origin --tags

# Please create your own branch